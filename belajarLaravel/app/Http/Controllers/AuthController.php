<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('halaman.Register');
    }

    public function kirim(Request $request)
    {
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];

        return view('halaman.Welcome', compact('namadepan', 'namabelakang'));
    }
}
