@extends('layout.master')
    
@section('judul')
Halaman Biodata
@endsection

@section('content')
    <form action="/kirim" method="post">
    @csrf
        <label>First name:</label> <br>
        <input type="text" name="fname"> <br> <br>
        <label>Last name:</label> <br>
        <input type="text" name="lname"> <br> <br>
        <label>Gender</label> <br>
        <input type="radio" name="Jenis Kelamin" value="1"> Male <br> 
        <input type="radio" name="Jenis Kelamin" value="2"> Female <br>
        <input type="radio" name="Jenis Kelamin" value="3"> Other <br> <br>
        <label>Nationality:</label> <br>
        <select name="Negara">
            <option value="1">Indonesia</option>
            <option value="2">Malaysia</option>
            <option value="3">USA</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="Bahasa"> Bahasa Indonesia <br>
        <input type="checkbox" name="Bahasa"> English <br>
        <input type="checkbox" name="Bahasa"> Other <br> <br>
        <label>Bio:</label> <br>
        <textarea name="message" rows="10" cols="30"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
    
    

